csstring
========

Csstring is a C library of very simple ASCII string functions.

  Copyright (C) 2010-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
  
  This file is part of csstring.
 
  csstring is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published
  by the Free Software Foundation; either version 2.1 of the License, or
  (at your option) any later version.
 
  csstring is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.
 
  You should have received a copy of the GNU Lesser General Public
  License along with csstring; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
  USA

Introduction
------------

Csstring is a C library of very simple ASCII string functions. It was written
on Slackware Linux 15.0.

Why
---

I needed some string functions that were slightly more high-level than those
provided in the standard C library. I need to be able to create strings
dynamically and manipulate them at run time.

Building and Installing
-----------------------

csstring depends on timber, cutl and mal. You can download and install them
manually.

1. https://gitlab.com/spaghetti-shed/timber.git
2. https://gitlab.com/spaghetti-shed/mal.git
3. https://gitlab.com/spaghetti-shed/cutl.git

To perform a manual install of csstring, the steps are:
Obtain the source from gitlab

    git clone https://gitlab.com/spaghetti-shed/csstring.git

In the csstring directory type 'make' to compile the code and run the unit tests

    cd csstring
    make

As root, type 'make install' to install the binaries and headers under /usr/local and update the dynamic linker cache

    su
    make install
    /sbin/ldconfig

For the adventurous, the cowboy builder (https://gitlab.com/spaghetti-shed/cowboy) can be used to download, build and install the dependencies automatically.

To build using the cowboy builder, and to download, build and install dependencies automatically (WARNING: runs make install as root):

    cowboy --dangerous csstring_project.cow


Use
---

Start by looking at the unit tests (csstring_test.c) for clues.

Frequently Anticipated Questions
--------------------------------

Q1: It doesn't do very much, does it?  
A1: No. 

Q2: Why bother?  
A2: I have some other code to write that needs to be able to do string
    operations.

Q3: Shouldn't you be writing this in C++?  
A3: Wash your mouth out with soap and water.

Q4: Why didn't you just use package ${FOO}?  
A4: Where's the fun in that?

Q5: Is it fast?  
A5: No idea. I don't even know how buggy it is yet.

TO-DO
-----

* More operations.
* Documentation.
* Optimisations.
* Compile-time macros to turn off memory checks.

