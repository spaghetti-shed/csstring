/******************************************************************************
 * csstring - Cheap Scottish Strings.
 *
 * Thu Feb  2 21:12:29 GMT 2017
 *
 * Copyright (C) 2010-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of csstring.
 *
 * csstring is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * csstring is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with csstring; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2017-02-02 Extracted from project.c.
 * 2019-03-16 Add _create_from_many().
 * 2022-12-26 Extracted from skelly and renamed from cs_string to csstring.
 *            Add csstring_insert().
 *            Add csstring_substring().
 * 2022-12-18 Add csstring_find().
 * 2024-09-13 Add _with_labels() functions similar to MAL to store file,
 *            function and line where strings are created and destroyed.
 ******************************************************************************/

#include <string.h>
#include <stdint.h>
#include "timber.h"
#include "mal.h"
#include "csstring.h"

char *csstring_create_with_content(char *s)
{
    char *t = NULL;

    if(NULL == s)
    {
        TDR_DIAG_ERROR("NULL pointer supplied.\n");
    }
    else
    {
        t = MAL_MALLOC(sizeof(char) * (strlen(s) + 1));
        if(NULL == t)
        {
            TDR_DIAG_ERROR("MAL_MALLOC() failed.\n");
        }
        else
        {
            strcpy(t, s);
        }
    }

    return t;
}

char *csstring_create_with_content_with_labels(const char *file, const char *function, int32_t line, char *s)
{
    char *t = NULL;

    if(NULL == s)
    {
        TDR_DIAG_ERROR("NULL pointer supplied.\n");
    }
    else
    {
        t = mal_malloc_with_labels(sizeof(char) * (strlen(s) + 1), file, function, line);
        if(NULL == t)
        {
            TDR_DIAG_ERROR("MAL_MALLOC() failed.\n");
        }
        else
        {
            strcpy(t, s);
        }
    }

    return t;
}

char *csstring_create_from_two(char *s1, char *s2)
{
    char *t = NULL;
    int32_t size;

    if(NULL == s1)
    {
        TDR_DIAG_ERROR("1st arg is NULL.\n");
    }
    else if(NULL == s2)
    {
        TDR_DIAG_ERROR("2nd arg is NULL.\n");
    }
    else
    {
        size = strlen(s1) + strlen(s2) + 1;
        t = MAL_MALLOC(sizeof(char) * size);
        if(NULL == t)
        {
            TDR_DIAG_ERROR("MAL_MALLOC() failed.\n");
        }
        else
        {
            strcpy(t, s1);
            strcat(t, s2);
        }
    }

    return t;
}

char *csstring_create_from_two_with_labels(const char *file, const char *function, int32_t line, char *s1, char *s2)
{
    char *t = NULL;
    int32_t size;

    if(NULL == s1)
    {
        TDR_DIAG_ERROR("1st arg is NULL.\n");
    }
    else if(NULL == s2)
    {
        TDR_DIAG_ERROR("2nd arg is NULL.\n");
    }
    else
    {
        size = strlen(s1) + strlen(s2) + 1;
        t = mal_malloc_with_labels(sizeof(char) * size, file, function, line);
        if(NULL == t)
        {
            TDR_DIAG_ERROR("MAL_MALLOC() failed.\n");
        }
        else
        {
            strcpy(t, s1);
            strcat(t, s2);
        }
    }

    return t;
}

char *csstring_ends_with(char *s1, char *s2)
{
    char *end = NULL;
    char *t;
    int32_t s1_len;
    int32_t s2_len;

    if(NULL == s1)
    {
        TDR_DIAG_ERROR("1st arg is NULL.\n");
    }
    else if(NULL == s2)
    {
        TDR_DIAG_ERROR("2nd arg is NULL.\n");
    }
    else if((s2_len = strlen(s2)) < 1)
    {
    }
    else if((s1_len = strlen(s1)) < s2_len)
    {
    }
    else
    {
        t = &s1[s1_len - s2_len];
        if( ! strcmp(t, s2))
        {
            end = t;
        }
    }

    return end;
}

char *csstring_begins_with(char *s1, char *s2)
{
    char *start = NULL;
    int32_t s1_len;
    int32_t s2_len;

    if(NULL == s1)
    {
        TDR_DIAG_ERROR("1st arg is NULL.\n");
    }
    else if(NULL == s2)
    {
        TDR_DIAG_ERROR("2nd arg is NULL.\n");
    }
    else if((s2_len = strlen(s2)) < 1)
    {
    }
    else if((s1_len = strlen(s1)) < s2_len)
    {
    }
    else
    {
        if( ! strncmp(s1, s2, (size_t)s2_len))
        {
            start = s1;
        }
    }

    return start;
}

int32_t csstring_destroy(char **s)
{
    int32_t result = -1;

    if((NULL != s) && (NULL != *s))
    {
        MAL_FREE(*s);
        *s = NULL;
        result = 0;
    }
    else
    {
        TDR_DIAG_ERROR("NULL pointer supplied.\n");
    }

    return result;
}

int32_t csstring_destroy_with_labels(const char *file, const char *function, int32_t line, char **s)
{
    int32_t result = -1;

    if((NULL != s) && (NULL != *s))
    {
        mal_free_with_labels(*s, file, function, line);
        *s = NULL;
        result = 0;
    }
    else
    {
        TDR_DIAG_ERROR("NULL pointer supplied.\n");
    }

    return result;
}



char *csstring_create_from_many(int n, ...)
{
    char *t = NULL;
    int32_t count;
    int32_t size = 1;

    va_list ap;

    if(n < 1)
    {
        TDR_DIAG_ERROR("Invalid count %d.\n", n);
    }
    else
    {
        va_start(ap, n);
        for(count = 0; count < n; count++)
        {
            size += strlen(va_arg(ap, char *));
        }
        va_end(ap);

        if (NULL == (t = MAL_MALLOC(sizeof(char) * size)))
        {
            TDR_DIAG_ERROR("MAL_MALLOC() failed.\n");
        }
        else
        {
            *t = '\0';
            va_start(ap, n);
            for(count = 0; count < n; count++)
            {
                strcat(t, va_arg(ap, char *));
            }
            va_end(ap);
        }
    }

    return t;
}

char *csstring_create_from_many_with_labels(const char *file, const char *function, int32_t line, int n, ...)
{
    char *t = NULL;
    int32_t count;
    int32_t size = 1;

    va_list ap;

    if(n < 1)
    {
        TDR_DIAG_ERROR("Invalid count %d.\n", n);
    }
    else
    {
        va_start(ap, n);
        for(count = 0; count < n; count++)
        {
            size += strlen(va_arg(ap, char *));
        }
        va_end(ap);

        if (NULL == (t = mal_malloc_with_labels(sizeof(char) * size, file, function, line)))
        {
            TDR_DIAG_ERROR("MAL_MALLOC() failed.\n");
        }
        else
        {
            *t = '\0';
            va_start(ap, n);
            for(count = 0; count < n; count++)
            {
                strcat(t, va_arg(ap, char *));
            }
            va_end(ap);
        }
    }

    return t;
}

/*
 * csstring_insert
 * char *s1: String in which to insert second string.
 * char *s2: String to insert in s1.
 * position: Positin in s1 (starting at 0) in which to insert s2.
 * returns:  New string with s2 inserted in s1 as specified on NULL on failure.
 */
char   *csstring_insert(char* s1, char *s2, int32_t position)
{
    char *t = NULL;
    size_t s1_len;

    if(NULL == s1)
    {
        TDR_DIAG_ERROR("1st arg is NULL.\n");
    }
    else if(NULL == s2)
    {
        TDR_DIAG_ERROR("2nd arg is NULL.\n");
    }
    else if (position < 0)
    {
        TDR_DIAG_ERROR("Insertion position negative (%d).\n", position);
    } 
    else if (position > (s1_len = strlen(s1)))
    {
        TDR_DIAG_ERROR("Insertion position beyond end of string (%d).\n", position);
    }
    else if (NULL == (t = MAL_MALLOC(s1_len + strlen(s2) + 1)))
    {
        TDR_DIAG_ERROR("MAL_MALLOC() failed.\n");
    }
    else
    {
        *t = '\0';
        strncat(t, s1, position);
        strcat(t, s2);
        strcat(t, s1 + position);
    }

    return t;
}

char   *csstring_insert_with_labels(const char *file, const char *function, int32_t line, char* s1, char *s2, int32_t position)
{
    char *t = NULL;
    size_t s1_len;

    if(NULL == s1)
    {
        TDR_DIAG_ERROR("1st arg is NULL.\n");
    }
    else if(NULL == s2)
    {
        TDR_DIAG_ERROR("2nd arg is NULL.\n");
    }
    else if (position < 0)
    {
        TDR_DIAG_ERROR("Insertion position negative (%d).\n", position);
    } 
    else if (position > (s1_len = strlen(s1)))
    {
        TDR_DIAG_ERROR("Insertion position beyond end of string (%d).\n", position);
    }
    else if (NULL == (t = mal_malloc_with_labels(s1_len + strlen(s2) + 1, file, function, line)))
    {
        TDR_DIAG_ERROR("MAL_MALLOC() failed.\n");
    }
    else
    {
        *t = '\0';
        strncat(t, s1, position);
        strcat(t, s2);
        strcat(t, s1 + position);
    }

    return t;
}

char   *csstring_substring(char* s1, int32_t start_index, int32_t end_index)
{
    char *t = NULL;
    size_t s1_len;

    if(NULL == s1)
    {
        TDR_DIAG_ERROR("1st arg is NULL.\n");
    }
    else if (start_index < 0)
    {
        TDR_DIAG_ERROR("Start index negative (%d).\n", start_index);
    } 
    else if (start_index >= (s1_len = strlen(s1)))
    {
        TDR_DIAG_ERROR("Start index (%d) beyond end of string (%zd).\n", start_index, (s1_len - 1));
    }
    else if (end_index < start_index)
    {
        TDR_DIAG_ERROR("Start index (%d) beyond end index (%d).\n", start_index, end_index);
    }
    else if (end_index >= s1_len)
    {
        TDR_DIAG_ERROR("End index (%d) beyond end of string (%zd).\n", end_index, (s1_len - 1));
    }
    else if (NULL == (t = MAL_MALLOC((end_index - start_index) + 1 + 1)))
    {
        TDR_DIAG_ERROR("MAL_MALLOC() failed.\n");
    }
    else
    {
        strncpy(t, s1 + start_index, (end_index - start_index) + 1);
        *(t + ((end_index - start_index) + 1)) = '\0';
    }

    return t;
}

char   *csstring_substring_with_labels(const char *file, const char *function, int32_t line, char* s1, int32_t start_index, int32_t end_index)
{
    char *t = NULL;
    size_t s1_len;

    if(NULL == s1)
    {
        TDR_DIAG_ERROR("1st arg is NULL.\n");
    }
    else if (start_index < 0)
    {
        TDR_DIAG_ERROR("Start index negative (%d).\n", start_index);
    } 
    else if (start_index >= (s1_len = strlen(s1)))
    {
        TDR_DIAG_ERROR("Start index (%d) beyond end of string (%zd).\n", start_index, (s1_len - 1));
    }
    else if (end_index < start_index)
    {
        TDR_DIAG_ERROR("Start index (%d) beyond end index (%d).\n", start_index, end_index);
    }
    else if (end_index >= s1_len)
    {
        TDR_DIAG_ERROR("End index (%d) beyond end of string (%zd).\n", end_index, (s1_len - 1));
    }
    else if (NULL == (t = mal_malloc_with_labels((end_index - start_index) + 1 + 1, file, function, line)))
    {
        TDR_DIAG_ERROR("MAL_MALLOC() failed.\n");
    }
    else
    {
        strncpy(t, s1 + start_index, (end_index - start_index) + 1);
        *(t + ((end_index - start_index) + 1)) = '\0';
    }

    return t;
}

int32_t csstring_find_internal(char *s1, char *s2)
{
    int32_t result = -1;
    int32_t s1_len;
    int32_t s2_len;
    int32_t end;
    int32_t count;

#ifdef DEBUG
    printf("s1: %s, s2: %s\n", s1, s2);
#endif

    if ((s2_len = strlen(s2)) > (s1_len = strlen(s1)))
    {
    }
    else
    {
        count = 1;
        end = s2_len - 1;
        while ((end >= 0) && (*(s2 + end) == *(s1 + end)))
        {
#ifdef DEBUG
            printf("s1:%c s2:%c\n", *(s1 + end), *(s2 + end));
#endif
            end--;
            count++;
        }

#ifdef DEBUG
        printf("count: %d\n", count);
#endif

        if (-1 == end)
        {
            result = 0;
        }
        else
        {
            result = count + csstring_find_internal(s1 + count, s2);
        }
    }

    return result;
}

/*
 * csstring_find
 * Find the fist occurence of string s2 in string s1 and return the index at
 * which s2 was found, or -1 on failure. Does not include teriminating nulls in
 * the search.
 */
int32_t csstring_find(char *s1, char *s2)
{
    int32_t result = -1;
    int32_t s1_len;
    int32_t s2_len;

    if(NULL == s1)
    {
        TDR_DIAG_ERROR("1st arg is NULL.\n");
    }
    else if(NULL == s2)
    {
        TDR_DIAG_ERROR("2nd arg is NULL.\n");
    }
    else if ((s2_len = strlen(s2)) > (s1_len = strlen(s1)))
    {
        TDR_DIAG_ERROR("Search string longer (%d) than source (%d).\n", s2_len, s1_len);
    }
    else
    {
        result = csstring_find_internal(s1, s2);
    }

    return result;
}

