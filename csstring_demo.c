/*
 * csstring_demo.c - Minimal demo program to exercise the macro API.
 *
 * Fri Sep 13 20:50:19 BST 2024
 *
 * Copyright (C) 2010-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of csstring.
 *
 * csstring is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * csstring is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with csstring; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2024-09-13 Initial creation.
 */

#include <stdio.h>
#include <stdint.h>
#include "mal.h"
#include "csstring.h"

#define NOBJECTS    1024

int32_t main(int argc, char *argv[])
{
    MAL_OPEN(NOBJECTS);
    char *s1 = CSSTRING_CREATE_WITH_CONTENT("Hello");
    char *s2 = CSSTRING_CREATE_WITH_CONTENT(",");
    char *s3 = CSSTRING_CREATE_WITH_CONTENT(" World!");
    char *s4 = CSSTRING_CREATE_WITH_CONTENT("\n");
    char *s5;
    char *s6;
    char *s7;
    char *s8;
    int32_t nspare;


    printf("s1 = %s\n", s1);
    printf("s2 = %s\n", s2);
    printf("s3 = %s\n", s3);
    printf("s4 = %s\n", s4);

    s5 = CSSTRING_CREATE_FROM_TWO(s1, s2);
    printf("s5 = %s\n", s5);

    s6 = CSSTRING_CREATE_FROM_MANY(3, s5, s3, s4);
    printf("s6 = %s\n", s6);

    s7 = CSSTRING_INSERT(s6, " there", 5);
    printf("s7 = %s\n", s7);

    s8 = CSSTRING_SUBSTRING(s7, 6, 10);
    printf("s8 = %s\n", s8);

    CSSTRING_DESTROY(&s8);
    CSSTRING_DESTROY(&s7);
    CSSTRING_DESTROY(&s6);
    CSSTRING_DESTROY(&s5);
    CSSTRING_DESTROY(&s4);
    CSSTRING_DESTROY(&s3);
    CSSTRING_DESTROY(&s2);
    CSSTRING_DESTROY(&s1);

    nspare = MAL_CLOSE();
    printf("Number of unreclaimed pointers: %d\n", nspare);

    return 0;
}

