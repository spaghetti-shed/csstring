/******************************************************************************
 * csstring (header) - Cheap Scottish Strings (header).
 *
 * Thu Feb  2 21:12:29 GMT 2017
 *
 * Copyright (C) 2010-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of csstring.
 *
 * csstring is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * csstring is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with csstring; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2017-02-02 Extracted from project.h.
 * 2019-03-16 Add _create_from_many().
 * 2022-12-26 Extracted from skelly and renamed from cs_string to csstring.
 *            Add csstring_insert().
 *            Add csstring_substring().
 * 2022-12-18 Add csstring_find().
 * 2024-09-13 Add _with_labels() functions similar to MAL to store file,
 *            function and line where strings are created and destroyed.
 ******************************************************************************/

#ifndef __CS_STRING_H__
#define __CS_STRING_H__

#include <stdarg.h>

#ifndef CSSTRING_PASSTHROUGH

#define CSSTRING_CREATE_WITH_CONTENT(s) \
    csstring_create_with_content_with_labels(__FILE__, __FUNCTION__, __LINE__, s)

#define CSSTRING_CREATE_FROM_TWO(s1, s2) \
    csstring_create_from_two_with_labels(__FILE__, __FUNCTION__, __LINE__, s1, s2)

#define CSSTRING_ENDS_WITH(s1, s2) \
    csstring_ends_with(s1, s2)

#define CSSTRING_BEGINS_WITH(s1, s2) \
    csstring_begins_with(s1, s2)

#define CSSTRING_DESTROY(s) \
    csstring_destroy_with_labels(__FILE__, __FUNCTION__, __LINE__, s)

#define CSSTRING_CREATE_FROM_MANY(n, ...) \
    csstring_create_from_many_with_labels(__FILE__, __FUNCTION__, __LINE__, n, ## __VA_ARGS__)

#define CSSTRING_INSERT(s1, s2, position) \
    csstring_insert_with_labels(__FILE__, __FUNCTION__, __LINE__, s1, s2, position)

#define CSSTRING_SUBSTRING(s1, start_index, end_index) \
    csstring_substring_with_labels(__FILE__, __FUNCTION__, __LINE__, s1, start_index, end_index)

#else

#define CSSTRING_CREATE_WITH_CONTENT(s) \
    csstring_create_with_content(s)

#define CSSTRING_CREATE_FROM_TWO(s1, s2) \
    csstring_create_from_two(s1, s2)

#define CSSTRING_ENDS_WITH(s1, s2) \
    csstring_ends_with(s1, s2)

#define CSSTRING_BEGINS_WITH(s1, s2) \
    csstring_begins_with(s1, s2)

#define CSSTRING_DESTROY(s) \
    csstring_destroy(s)

#define CSSTRING_CREATE_FROM_MANY(n, ...) \
    csstring_create_from_many(n, ## __VA_ARGS__)

#define CSSTRING_INSERT(s1, s1, position) \
    csstring_insert(s1, s2, position)

#define CSSTRING_SUBSTRING(s1, start_index, end_index) \
    csstring_substring(s1, start_index, end_index)

#endif


#ifdef __cplusplus
extern "C"
{
#endif

char   *csstring_create_with_content(char *s);
char   *csstring_create_with_content_with_labels(const char *file, const char *function, int32_t line, char *s);

char   *csstring_create_from_two(char *s1, char *s2);
char   *csstring_create_from_two_with_labels(const char *file, const char *function, int32_t line, char *s1, char *s2);

char   *csstring_ends_with(char *s1, char *s2);
char   *csstring_begins_with(char *s1, char *s2);

int32_t csstring_destroy(char **s);
int32_t csstring_destroy_with_labels(const char *file, const char *function, int32_t line, char **s);

char   *csstring_create_from_many(int32_t n, ...);
char   *csstring_create_from_many_with_labels(const char *file, const char *function, int32_t line, int32_t n, ...);

/*
 * csstring_insert
 * char *s1: String in which to insert second string.
 * char *s2: String to insert in s1.
 * position: Positin in s1 (starting at 0) in which to insert s2.
 * returns:  New string with s2 inserted in s1 as specified on NULL on failure.
 */
char   *csstring_insert(char* s1, char *s2, int32_t position);
char   *csstring_insert_with_labels(const char *file, const char *function, int32_t line, char* s1, char *s2, int32_t position);

char   *csstring_substring(char* s1, int32_t start_index, int32_t end_index);
char   *csstring_substring_with_labels(const char *file, const char *function, int32_t line, char* s1, int32_t start_index, int32_t end_index);

/*
 * csstring_find
 * Find the fist occurence of string s2 in string s1 and return the index at
 * which s2 was found, or -1 on failure.
 */
int32_t csstring_find(char *s1, char *s2);

#ifdef __cplusplus
}
#endif

#endif /* __CS_STRING_H__ */

