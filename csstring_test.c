/*****************************************************************************
 * csstring (test) - Cheap Scottish Strings (test).
 * Wed Jun 21 16:17:38 BST 2017
 *
 * Copyright (C) 2017-2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of csstring.
 *
 * csstring is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * csstring is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with csstring; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2017-06-21: Initial creation. Extracted from module_list_test.c.
 *             Note that this tests the wrong things.
 * 2019-03-16: Create from many.
 * 2022-12-26: Extracted from skelly and renamed from cs_string to csstring.
 *             Add csstring_insert().
 *             Add csstring_substring().
 * 2022-12-18: Add csstring_find().
 * 2023-01-04: Replace tabs with spaces.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <malloc.h>
#include <string.h>

#include "csstring.h"

#include "cutl.h"
#include "mal.h"

void test_strings(void);
void test_csstring_ends_with(void);
void test_csstring_begins_with(void);
void test_csstring_create_from_many(void);
void test_csstring_insert(void);
void test_csstring_substring(void);
void test_csstring_find(void);

cutl_test_case_t tests[] =
{
    { "strings",                   test_strings                   },
    { "csstring_ends_with",        test_csstring_ends_with        },
    { "csstring_begins_with",      test_csstring_begins_with      },
    { "csstring_create_from_many", test_csstring_create_from_many },
    { "csstring_insert",           test_csstring_insert           },
    { "csstring_substring",        test_csstring_substring        },
    { "csstring_find",             test_csstring_find             },
};

#define NO_OF_OBJECTS   1024

void module_object_test_case_setup(void)
{
    MAL_OPEN(NO_OF_OBJECTS);
}

void module_object_test_case_teardown(void)
{
    int32_t nspare;

    nspare = MAL_CLOSE();
    if (nspare != 0)
    {
        printf("Number of unreclaimed pointers: %d\n", nspare);
    } 
}

int main(int argc, char *argv[])
{
    cutl_set_test_lib("csstring_test", tests, NO_OF_ELEMENTS(tests));
    cutl_set_cutl_test_setup(module_object_test_case_setup);
    cutl_set_cutl_test_teardown(module_object_test_case_teardown);
    cutl_get_options(argc, argv);
    cutl_run_tests();

    return 0;
}

void test_strings()
{
    char *s1, *s2, *s3, *s4;

    s1 = NULL;
    s2 = NULL;

    s1 = csstring_create_with_content(NULL);
    TEST_PTR_NULL("cstring_create_with_content", s1);

    s1 = csstring_create_with_content("");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);
    TEST_STR_EQUAL("csstring_create_with_content", "", "");
    TEST_STR_EQUAL("csstring_create_with_content", s1, "");
    TEST_INT32_EQUAL("csstring_create_with_content", -1, csstring_destroy(NULL));
    TEST_INT32_EQUAL("csstring_create_with_content", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_create_with_content", s1);

    s1 = csstring_create_with_content("Hello, world!");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);
    TEST_STR_EQUAL("csstring_create_with_content", s1, "Hello, world!");
    TEST_INT32_EQUAL("csstring_create_with_content", 0,  csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_create_with_content", s1);

    s2 = csstring_create_from_two(NULL, NULL);
    TEST_PTR_NULL("csstring_create_from_two", s2);
    s2 = csstring_create_from_two(NULL, "");
    TEST_PTR_NULL("csstring_create_from_two", s2);
    s2 = csstring_create_from_two("", NULL);
    TEST_PTR_NULL("csstring_create_from_two", s2);

    s2 = csstring_create_from_two("", "");
    TEST_PTR_NOT_NULL("csstring_create_from_two", s2);
    TEST_STR_EQUAL("csstring_create_from_two", s2, "");
    TEST_INT32_EQUAL("csstring_create_from_two", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_create_from_two", s2);

    s2 = csstring_create_from_two("fo", "o");
    TEST_PTR_NOT_NULL("csstring_create_from_two", s2);
    TEST_STR_EQUAL("csstring_create_from_two", s2, "foo");
    TEST_INT32_EQUAL("csstring_create_from_two", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_create_from_two", s2);

    s3 = csstring_create_from_two("", "bar");
    TEST_PTR_NOT_NULL("csstring_create_from_two", s3);
    TEST_STR_EQUAL("csstring_create_from_two", s3, "bar");
    TEST_INT32_EQUAL("csstring_create_from_two", 0, csstring_destroy(&s3));
    TEST_PTR_NULL("csstring_create_from_two", s3);

    s4 = csstring_create_from_two("banana", "");
    TEST_PTR_NOT_NULL("csstring_create_from_two", s4);
    TEST_STR_EQUAL("csstring_create_from_two", s4, "banana");
    TEST_INT32_EQUAL("csstring_create_from_two", 0, csstring_destroy(&s4));
    TEST_PTR_NULL("csstring_create_from_two", s4);
}

void test_csstring_ends_with()
{
    char *s1, *s2, *s3, *s4;
    char *e;

    s1 = "x";
    s2 = "hex";
    s3 = "this is a big long test";
    s4 = "long test";

    e = csstring_ends_with(NULL, NULL);
    TEST_PTR_NULL("csstring_ends_with", e);

    e = csstring_ends_with(NULL, s1);
    TEST_PTR_NULL("csstring_ends_with", e);

    e = csstring_ends_with(s1, NULL);
    TEST_PTR_NULL("csstring_ends_with", e);

    e = csstring_ends_with(s1, s1);
    TEST_PTR_NOT_NULL("csstring_ends_with", e);
#ifdef VERBOSE
    printf("end: %s\n", e);
#endif

    e = csstring_ends_with("", s1);
    TEST_PTR_NULL("csstring_ends_with", e);

    e = csstring_ends_with(s1, "");
    TEST_PTR_NULL("csstring_ends_with", e);

    e = csstring_ends_with(s2, s1);
    TEST_PTR_NOT_NULL("csstring_ends_with", e);
#ifdef VERBOSE
    printf("end: %s\n", e);
#endif

    e = csstring_ends_with(s1, s2);
    TEST_PTR_NULL("csstring_ends_with", e);

    e = csstring_ends_with(s3, s4);
    TEST_PTR_NOT_NULL("csstring_ends_with", e);
#ifdef VERBOSE
    printf("end: %s\n", e);
#endif

    e = csstring_ends_with(s4, s3);
    TEST_PTR_NULL("csstring_ends_with", e);

    e = csstring_ends_with(s3, s3);
    TEST_PTR_NOT_NULL("csstring_ends_with", e);
#ifdef VERBOSE
    printf("end: %s\n", e);
#endif

    e = csstring_ends_with(s4, s4);
    TEST_PTR_NOT_NULL("csstring_ends_with", e);
#ifdef VERBOSE
    printf("end: %s\n", e);
#endif
}

void test_csstring_begins_with()
{
    char *s1, *s2, *s3, *s4;
    char *e;

    s1 = "h";
    s2 = "hex";
    s3 = "this is a big long test";
    s4 = "this is";

    e = csstring_begins_with(NULL, NULL);
    TEST_PTR_NULL("csstring_begins_with", e);

    e = csstring_begins_with(NULL, s1);
    TEST_PTR_NULL("csstring_begins_with", e);

    e = csstring_begins_with(s1, NULL);
    TEST_PTR_NULL("csstring_begins_with", e);

    e = csstring_begins_with(s1, s1);
    TEST_PTR_NOT_NULL("csstring_begins_with", e);
#ifdef VERBOSE
    printf("start: %s\n", e);
#endif

    e = csstring_begins_with("", s1);
    TEST_PTR_NULL("csstring_begins_with", e);

    e = csstring_begins_with(s1, "");
    TEST_PTR_NULL("csstring_begins_with", e);

    e = csstring_begins_with(s2, s1);
    TEST_PTR_NOT_NULL("csstring_begins_with", e);
#ifdef VERBOSE
    printf("start: %s\n", e);
#endif

    e = csstring_begins_with(s1, s2);
    TEST_PTR_NULL("csstring_begins_with", e);

    e = csstring_begins_with(s3, s4);
    TEST_PTR_NOT_NULL("csstring_begins_with", e);
#ifdef VERBOSE
    printf("start: %s\n", e);
#endif

    e = csstring_begins_with(s4, s3);
    TEST_PTR_NULL("csstring_begins_with", e);

    e = csstring_begins_with(s3, s3);
    TEST_PTR_NOT_NULL("csstring_begins_with", e);
#ifdef VERBOSE
    printf("start: %s\n", e);
#endif

    e = csstring_begins_with(s4, s4);
    TEST_PTR_NOT_NULL("csstring_begins_with", e);
#ifdef VERBOSE
    printf("start: %s\n", e);
#endif
}

void test_csstring_create_from_many()
{
    char *s1, *s2, *s3, *s4;

    s1 = csstring_create_from_many(0);
    TEST_PTR_NULL("csstring_create_from_many(0)", s1);
    s1 = csstring_create_from_many(0, "");
    TEST_PTR_NULL("csstring_create_from_many(0, \"\")", s1);
    s1 = csstring_create_from_many(0, "", "");
    TEST_PTR_NULL("csstring_create_from_many(0, \"\", \"\")", s1);

    s2 = csstring_create_from_many(1, "this");
    TEST_PTR_NOT_NULL("csstring_create_from_many(1, \"this\")", s2);
    TEST_STR_EQUAL("csstring_create_from_many(1, \"this\")", s2, "this");
    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s2 = csstring_create_from_many(2, "this", " is");
    TEST_PTR_NOT_NULL("banana", s2);
    TEST_STR_EQUAL("banana", s2, "this is");
    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s3 = csstring_create_from_many(3, "this ", "is ", "a ");
    TEST_PTR_NOT_NULL("banana", s3);
    TEST_STR_EQUAL("banana", s3, "this is a ");
    TEST_INT32_EQUAL("banana", 0, csstring_destroy(&s3));
    TEST_PTR_NULL("banana", s3);

    s4 = csstring_create_from_many(6, "this ", "is ", "a ", "big ", "long ", "test");
    TEST_PTR_NOT_NULL("banana", s4);
    TEST_STR_EQUAL("banana", s4, "this is a big long test");
    TEST_INT32_EQUAL("banana", 0, csstring_destroy(&s4));
    TEST_PTR_NULL("banana", s4);
}

void test_csstring_insert(void)
{
    char *s1;
    char *s2;

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_insert(s1, "X", 0);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_insert", "Xabcde", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_insert(s1, "X", 1);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_insert", "aXbcde", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_insert(s1, "X", 2);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_insert", "abXcde", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_insert(s1, "X", 3);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_insert", "abcXde", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_insert(s1, "X", 4);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_insert", "abcdXe", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_insert(s1, "X", 5);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_insert", "abcdeX", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);
    
    /*
     * Insertion index out of range.
     */
    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_insert(s1, "X", -1);
    TEST_PTR_NULL("s2", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_insert(s1, "X", 6);
    TEST_PTR_NULL("s2", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    /*
     * Insert a long string.
     */
    s1 = csstring_create_with_content("Hello, World!");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_insert(s1, " there", 5);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_insert", "Hello there, World!", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);
}

void test_csstring_substring(void)
{
    char *s1;
    char *s2;

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 0, 4);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_substring", "abcde", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 0, 3);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_substring", "abcd", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 0, 2);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_substring", "abc", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 0, 1);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_substring", "ab", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 0, 0);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_substring", "a", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    /*
     * Get single characters at all other positions.
     */
    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 1, 1);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_substring", "b", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 2, 2);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_substring", "c", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 3, 3);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_substring", "d", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 4, 4);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_substring", "e", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    /*
     * Get two-character strings at all positions.
     */
    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 0, 1);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_substring", "ab", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 1, 2);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_substring", "bc", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 2, 3);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_substring", "cd", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 3, 4);
    TEST_PTR_NOT_NULL("s2", s2);
    TEST_STR_EQUAL("csstring_substring", "de", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    TEST_INT32_EQUAL("csstring_destroy(&s2)", 0, csstring_destroy(&s2));
    TEST_PTR_NULL("csstring_destroy(&s2)", s2);

    /*
     * Out of range indicies.
     */
    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, 4, 5);
    TEST_PTR_NULL("s2", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);

    s1 = csstring_create_with_content("abcde");
    TEST_PTR_NOT_NULL("csstring_create_with_content", s1);

    s2 = csstring_substring(s1, -1, 4);
    TEST_PTR_NULL("s2", s2);

    TEST_INT32_EQUAL("csstring_destroy(&s1)", 0, csstring_destroy(&s1));
    TEST_PTR_NULL("csstring_destroy(&s1)", s1);
}

void test_csstring_find(void)
{
    char *s1 = "This is a long string for testing.";
    int32_t index = 0;

    index = csstring_find(s1, "This");
    TEST_INT32_EQUAL("index", 0, index);

    index = csstring_find(s1, " is");
    TEST_INT32_EQUAL("index", 4, index);

    index = csstring_find(s1, "ing");
    TEST_INT32_EQUAL("index", 18, index);

    index = csstring_find(s1 + 19, "ing");
    TEST_INT32_EQUAL("index", 11, index);

    index = csstring_find(s1, ".");
    TEST_INT32_EQUAL("index", 33, index);

    index = csstring_find(s1, "This string is incredibly, ludicrously long.");
    TEST_INT32_EQUAL("index", -1, index);
}

