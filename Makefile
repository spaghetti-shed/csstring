#
# Makefile for csstring.
# Mon Dec 26 13:14:09 GMT 2022
#
# Copyright (C) 2010-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
#
# This file is part of csstring.
#
# csstring is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# csstring is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with csstring; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA
#
# Modification history:
# 2022-12-26 Extracted from skelly and renamed from cs_string to csstring.
# 2024-09-13 Add csstring_demo, demo for macro API.
#

CC=gcc
CFLAGS=-fPIC -Wall -Werror -O
SHAREDFLAGS=-shared
CPPTESTFLAGS=-DTESTING
INCLUDES=-I/usr/local/include
LIBS=-lcutl -ltimber -lmal
INSTALL_DIR=/usr/local
INSTALL_LIB=$(INSTALL_DIR)/lib64
INSTALL_INC=$(INSTALL_DIR)/include

all: main test shared demo

test: csstring_test
	./csstring_test

main: csstring

csstring: csstring.h csstring.c
	$(CC) $(CFLAGS) -c csstring.c

csstring_test: csstring_test.c csstring.c csstring.h
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -o csstring_testing.o -c csstring.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -c csstring_test.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -o csstring_test csstring_test.o csstring_testing.o $(LIBS)

shared: csstring
	$(CC) $(SHAREDFLAGS) -o libcsstring.so csstring.o -ltimber -lmal

demo: csstring
	$(CC) $(CFLAGS) -c csstring_demo.c
	$(CC) $(CFLAGS) -o csstring_demo csstring_demo.o csstring.o -ltimber -lmal

install:
	mkdir -p $(INSTALL_INC)
	cp csstring.h $(INSTALL_INC)
	mkdir -p $(INSTALL_LIB)
	cp libcsstring.so $(INSTALL_LIB)

uninstall:
	rm -f $(INSTALL_INC)/csstring.h
	mkdir -p $(INSTALL_LIB)
	rm -f $(INSTALL_LIB)/libcsstring.so

clean:
	rm -f *.o
	rm -f csstring_test
	rm -f csstring
	rm -f libcsstring.so
	rm -f csstring_demo.o
	rm -f csstring_demo

